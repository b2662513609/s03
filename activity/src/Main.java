import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please input a number:");
        int num = 0;
        try{
            num = in.nextInt();
        }catch (InputMismatchException e) {
            System.out.println("Please input numbers only!");
            return;
        }

        try{
            int total = 1;
            int ctr = 1;
            int multiplier = 1;
            while (ctr != num) {
                total = (multiplier * total) + total;
                ctr++;
                multiplier++;
            }
            System.out.println("The factorial of " + num + " is " + total + " done using while loop.");
        }catch (Exception e) {
            System.out.println("Use another input!");
        }

        try {
            int total = 1;
            for (int multiplier = 1; multiplier < num; multiplier++){
                total = (multiplier * total) + total;
            }
            System.out.println("The factorial of " + num + " is " + total + " done using for loop.");
        }catch (Exception e) {
            System.out.println("Use another input!");
        }
    }
}